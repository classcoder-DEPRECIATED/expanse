package com.classcoder.expanse;

import org.lwjgl.opengl.Display;

import com.classcoder.expanse.engine.*;
import com.classcoder.expanse.engine.grid.Grid;

public class Expanse implements Game {
	private GameManager gm;
	private Player player = new Player(new Coordinate2(0, 1));
	
	public Expanse() {
		// Just need to be able to construct.
	}
	
	public void frameLoop() {
		player.controls();
		Grid.putObject(player);
		
		Grid.render();
	}
	
	public void init(GameManager gm) {
		this.gm = gm;
		Display.setVSyncEnabled(true);
		this.gm.showWindow("Expanse", 512, 512);
		player.init(gm);
	}
	
	public void end() {
	} 
}
