package com.classcoder.expanse.engine;

import org.lwjgl.opengl.GL11;

public class Render {
	/**
	 * Renders a quad
	 * @param a Point a
	 * @param b Point b
	 * @param c Point c
	 * @param d Point d
	 * @param color
	 */
	public static void quad(Coordinate2 a, Coordinate2 b, Coordinate2 c, Coordinate2 d, RGBA color) {
		GL11.glColor4f(color.r, color.g, color.b, color.a);
		GL11.glBegin(GL11.GL_QUADS);
			GL11.glVertex2d(a.x, a.y);
			GL11.glVertex2d(b.x, b.y);
			GL11.glVertex2d(c.x, c.y);
			GL11.glVertex2d(d.x, d.y);
		GL11.glEnd();
	}
}
