package com.classcoder.expanse.engine;

public final class Coordinate2 {
	public double x;
	public double y;
	
	public Coordinate2(double x, double y) {
		this.x = x;
		this.y = y;
	}
	
	public Coordinate2 plusX(double amount) {
		return new Coordinate2(this.x+amount, this.y);
	}
	public Coordinate2 plusY(double amount) {
		return new Coordinate2(this.x, this.y+amount);
	}
}
