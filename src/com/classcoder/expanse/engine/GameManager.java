package com.classcoder.expanse.engine;

import org.lwjgl.Sys;
import org.lwjgl.opengl.Display;
import org.lwjgl.opengl.DisplayMode;
import org.lwjgl.opengl.GL11;

import com.classcoder.expanse.engine.grid.Grid;

public class GameManager {
	private Game game;
	
	private long lastFrame;
	public int fps;
	private long lastfps;
	public double delta;
	
	/**
	 * GameManager Constructor
	 */
	public GameManager(Game game) {
		this.game = game;
		this.game.init(this);
	}
	
	/**
	 * Shows LWJGL window
	 * 
	 * @param title Title of the window
	 * @param width Width of the window
	 * @param height Height of the window
	 */
	public void showWindow(String title, int width, int height) {
		System.out.println("\"showWindow\"");
		try {
			Display.setDisplayMode(new DisplayMode(width,height));
			Display.setTitle(title);
			Display.create();
			GL11.glMatrixMode(GL11.GL_PROJECTION);
			GL11.glLoadIdentity();
			GL11.glOrtho(0, Grid.width, 0, Grid.height, 1, -1);
			GL11.glMatrixMode(GL11.GL_MODELVIEW);
		} catch (Exception e) {
			ExceptionHandler.handle(e);
		}
	}
	
	/**
	 * @deprecated Replaced with Vsync.
	 */
	public int fpsLimit = 60;
	
	public void frame() {
		Grid.frame();
		GL11.glClear(GL11.GL_COLOR_BUFFER_BIT | GL11.GL_DEPTH_BUFFER_BIT);
		InputManager.update();
		game.frameLoop();
		Display.update();
		updateFPS();
		updateDelta();
		Display.sync(60);
	}
	
	public void end() {
		game.end();
		Display.destroy();
	}

	public void game() {
		updateDelta();
		lastfps = (Sys.getTime() * 1000) / Sys.getTimerResolution();
		while (!Display.isCloseRequested()) {
			frame();
		}
		end();
	}
	
	public void updateDelta() {
		long time = (Sys.getTime() * 1000) / Sys.getTimerResolution();
		delta = (time - lastFrame);
		lastFrame = time;
		System.out.format("Delta %f%n", delta);
	}
	
	public void updateFPS() {
		if (((Sys.getTime() * 1000) / Sys.getTimerResolution()) - lastfps > 1000) {
			System.out.format("FPS %d%n", fps);
			fps = 0;
			lastfps += 1000;
		}
		fps++;
	}
	
	public double byDelta(double amount) {
		return delta*amount;
	}
}
