package com.classcoder.expanse.engine;

import java.util.logging.Level;
import java.util.logging.Logger;

public class ExceptionHandler {
	/**
	 * Got an error? Feed it to the error bot!
	 * 
	 * @param e The exception to crash on
	 */
	@SuppressWarnings("deprecation")
	public static void handle(Exception e) {
		Logger.global.log(Level.SEVERE, "Oh, no! There's been an error:");
		e.printStackTrace();
		System.exit(1);
	}
}
