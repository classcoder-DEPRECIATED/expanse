package com.classcoder.expanse.engine;

import org.lwjgl.input.Keyboard;
import org.lwjgl.input.Mouse;

/**
 * Manages input for the game
 */
public class InputManager {
	public static MouseLocation mouseloc = new MouseLocation(0, 0);
	
	/**
	 * Check if mouse button is down
	 * 
	 * @param button 0 for right, 1 for left.
	 */
	public static boolean isMouseDown(int button) {
		return Mouse.isButtonDown(button);
	}
	
	/**
	 * Check if key is down
	 * 
	 * @param button Keyboard.&lt;button&gt;
	 */
	public static boolean isKeyDown(int button) {
		return Keyboard.isKeyDown(button);
	}
	
	static void update() {
		mouseloc.setX(Mouse.getX());
		mouseloc.setY(Mouse.getY());
	}
}
