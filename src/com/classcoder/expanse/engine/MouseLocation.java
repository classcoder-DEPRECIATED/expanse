package com.classcoder.expanse.engine;

public class MouseLocation {
	/**
	 * x and y locations of the mouse
	 */
	public int x, y;
	
	public MouseLocation(int x, int y) {
		setX(x);
		setY(y);
	}
	
	public int setX(int x) {
		return this.x = x;
	}
	public int getX() {
		return this.x;
	}
	public int setY(int y) {
		return this.y = y;
	}
	public int getY() {
		return this.y;
	}
}
