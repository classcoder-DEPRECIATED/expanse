package com.classcoder.expanse.engine;

/**
 * Game interface, implement this in your Game class.
 *
 */
public interface Game {
	/**
	 * Initialization code
	 * 
	 * @param The game's GameManager
	 */
	public void init(GameManager gm);
	/**
	 * Game frame loop
	 */
	public void frameLoop();
	/**
	 * Game destruction
	 */
	public void end();
}
