package com.classcoder.expanse.engine.grid;

import com.classcoder.expanse.engine.Coordinate2;
import com.classcoder.expanse.engine.Direction2;
import com.classcoder.expanse.engine.GameManager;
import com.classcoder.expanse.engine.RGBA;
import com.classcoder.expanse.engine.Render;

public class GridObject {
	public Coordinate2 location;
	public Coordinate2 renderLocation;
	public RGBA color;
	private int[] moveToDirection = new int[2];
	private boolean moveTo = false;
	private int moveToSpeed = 1;
	public boolean changeRenderLocation = true;
	
	public GridObject(Coordinate2 location, RGBA color) {
		this.location = location;
		if (this.changeRenderLocation == true) {
			this.renderLocation = location;
		}
		this.color = color;
	}
	
	public void render() {
		Grid.putObjectAt(location, this);
		Grid.render(location);
	}
	void render(GameManager gm) {
		if (moveTo == true) {
			for (int i=0; i<2; i++) {
				switch(moveToDirection[i]) {
				// case Direction2.UP:
				case 0:
					renderLocation = renderLocation.plusY(gm.byDelta(moveToSpeed)/100);
					break;
				// case Direction2.DOWN:
				case 1:
					renderLocation = renderLocation.plusY(gm.byDelta(0-moveToSpeed)/100);
					break;
				// case Direction2.LEFT:
				case 2:
					renderLocation = renderLocation.plusX(gm.byDelta(0-moveToSpeed)/100);
					break;
				// case Direction2.RIGHT:
				case 3:
					renderLocation = renderLocation.plusX(gm.byDelta(moveToSpeed)/100);
					break;
				}
			}
		}
		if (renderLocation.x == location.x && renderLocation.y == location.y) {
			moveTo = false;
		}
		Render.quad(renderLocation, renderLocation.plusX(1), renderLocation.plusX(1).plusY(1), renderLocation.plusY(1), this.color);
	}
	protected void moveTo(Coordinate2 location) {
		moveTo = true;
		char on = 0;
		if (location.y > this.location.y) {
			moveToDirection[on] = Direction2.UP;
			on++;
		}
		if (location.y < this.location.y) {
			moveToDirection[on] = Direction2.DOWN;
			on++;
		}
		if (location.x > this.location.x){
			moveToDirection[on] = Direction2.RIGHT;
			on++;
		}
		if (location.x < this.location.x) {
			moveToDirection[on] = Direction2.LEFT;
			on++;
		}
		this.location = location;
	}
}
