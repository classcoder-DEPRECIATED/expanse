package com.classcoder.expanse.engine.grid;

import com.classcoder.expanse.engine.Coordinate2;
import com.classcoder.expanse.engine.GameManager;

public class Grid {
	public static int width = 8, height = 8;
	
	private static GridObject[][] objects = new GridObject [width][height];
	
	private static GameManager gm;
	
	/**
	 * @param location The location to get the object at
	 */
	public static GridObject getObjectAt(Coordinate2 location) {
		return objects[(int) location.x][(int) location.y];
	}
	/**
	 * @param location The location to put the object at
	 * @param object The object to put
	 */
	public static void putObjectAt(Coordinate2 location, GridObject object) {
		if (location.x < width && location.y < height && !(location.x < 0) && !(location.y < 0)) {
			object.location = location;
			if (object.changeRenderLocation == true) {
				object.renderLocation = location;
			}
			objects[(int) location.x][(int) location.y] = object; 
		} else {
			if (location.x >= width) {
				location.x = width-1;
			}
			if (location.y >= height) {
				location.y = height-1;
			}
			if (location.x < 0) {
				location.x = 0;
			}
			if (location.y < 0) {
				location.y = 0;
			}
			putObjectAt(location, object);
		}
	}
	/**
	 * Puts object at object.location
	 * @param object The object to put
	 */
	public static void putObject(GridObject object) {
		putObjectAt(object.location, object);
	}
	/**
	 * @param location The location of the object to render
	 */
	public static void render(Coordinate2 location) {
		GridObject object = getObjectAt(location);
		object.render(gm);
	}
	public static void render() {
		for (int x = 0; x < objects.length; x++) {
			for (int y = 0; y < objects[x].length; y++) {
				if (getObjectAt(new Coordinate2(x,y)) == null) {
					continue;
				}
				render(new Coordinate2(x,y));
			}
		}
	}
	
	public static void init(GameManager gm) {
		Grid.gm = gm;
	}
	
	public static void frame() {
		objects = new GridObject [width][height];
	}
}
