package com.classcoder.expanse;

import org.lwjgl.input.Keyboard;

import com.classcoder.expanse.engine.Coordinate2;
import com.classcoder.expanse.engine.Direction2;
import com.classcoder.expanse.engine.GameManager;
import com.classcoder.expanse.engine.InputManager;
import com.classcoder.expanse.engine.RGBA;
import com.classcoder.expanse.engine.grid.GridObject;

public class Player extends GridObject {
	private GameManager gm;
	
	public Player(Coordinate2 location) {
		super(location, new RGBA(.2f, .7f, .5f, 1f));
	}
	
	public void controls() {
		if (InputManager.isKeyDown(Keyboard.KEY_D)) {
			move(Direction2.RIGHT, 1);
		}
		if (InputManager.isKeyDown(Keyboard.KEY_A)) {
			move(Direction2.LEFT, 1);
		}
		if (InputManager.isKeyDown(Keyboard.KEY_W)) {
			move(Direction2.UP, 1);
		}
		if (InputManager.isKeyDown(Keyboard.KEY_S)) {
			move(Direction2.DOWN, 1);
		}
	}
	
	public void move(int direction, int amount) {
//		System.out.format("Delta: %f%nFPS: %d%n", gm.delta, gm.fps);
		switch(direction) {
		// case Direction2.UP:
		case 0:
			this.moveTo(location.plusY(amount));
			break;
		// case Direction2.DOWN:
		case 1:
			this.moveTo(location.plusY(gm.byDelta(0-amount)/100));
			break;
		// case Direction2.LEFT:
		case 2:
			this.moveTo(location = location.plusX(gm.byDelta(0-amount)/100));
			break;
		// case Direction2.RIGHT:
		case 3:
			this.moveTo(location.plusX(gm.byDelta(amount)/100));
			break;
		}
	}

	public void init(GameManager gm) {
		this.gm = gm;
	}
}
