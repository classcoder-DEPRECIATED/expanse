package com.classcoder.expanse;

import com.classcoder.expanse.engine.Game;
import com.classcoder.expanse.engine.GameManager;
import com.classcoder.expanse.engine.grid.Grid;

public class Main {
	public static void main(String[] args) {
		Game game = new Expanse();
		GameManager gm = new GameManager(game);
		Grid.init(gm);
		gm.game();
	}
}