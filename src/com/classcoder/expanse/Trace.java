package com.classcoder.expanse;

import com.classcoder.expanse.engine.Coordinate2;
import com.classcoder.expanse.engine.RGBA;
import com.classcoder.expanse.engine.grid.GridObject;

public class Trace extends GridObject {

	public Trace(Coordinate2 location) {
		super(location, new RGBA(0.72156862745f, 0.72156862745f, 0.72156862745f, 1f));
	}

}
