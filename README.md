#COMPILING
Use Eclipse with the project then use the built-in build configurations.
#Issue Tracker
We use a JIRA issue tracker at https://classcoder.atlassian.net/browse/EX.

#License
    Expanse, a 2D video game
    Copyright (C) 2014  Zeb McCorkle

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.